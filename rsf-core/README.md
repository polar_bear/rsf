#RSF-Core

&emsp;&emsp;一个轻量化的分布式服务框架。典型的应用场景是，将同一个服务部署在多个`Server`上提供分布式的 request、response 消息通知。RSF是 `RemoteServiceFramework` 的缩写。

----------

&emsp;&emsp;该项目是RSF核心项目，RSF的所有核心代码都在这里。使用 `RSF-Core` 可以建立分布式服务，也可以作为两个单点之间 RPC 通信。